defmodule TimexWeb.IndigloManager do
  use GenServer

  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {:ok, %{ui_pid: ui, st: IndigloOff, counter: 0} }
  end

 def handle_info(:"top-right", %{ st: IndigloOff, ui_pid: ui } = state) do
  GenServer.cast(ui, :set_indiglo)
  { :noreply, %{state | st: IndigloOn} }
 end

 def handle_info(:"top-right", %{ st: IndigloOn, ui_pid: ui } = state) do
  GenServer.cast(ui, :set_indiglo)
  { :noreply, %{state | st: Waiting} }
 end

 def handle_info(:"top-right", %{ st: Waiting} = state) do
  Process.send_after(self(), Waiting2IndigloOff, 2000)

  { :noreply, %{state | st: Waiting} }
 end

 def handle_info(Waiting2IndigloOff, %{ st: Waiting, ui_pid: ui } = state) do
  GenServer.cast(ui, :unset_indiglo)
  {:noreply, state |> Map.put(:st, IndigloOff)}
 end

 def handle_info(:start_alarm, %{st: IndigloOff} = state) do
  Process.send_after(self(), :alarm_off, 1000)
  {:noreply, state}
 end

 def handle_info(:alarm_off, %{st: IndigloOff, ui_pid: ui, counter: counter} = state) when counter < 5 do
  Process.send_after(self(), :alarm_on, 1000)
  GenServer.cast(ui, :set_indiglo)
  {:noreply, state |> Map.put(:counter, counter + 1) |> Map.put(:st, IndigloOn) }
 end

 def handle_info(:alarm_on, %{st: IndigloOn, ui_pid: ui, counter: counter} = state) do
  GenServer.cast(ui, :unset_indiglo)
  if(counter != 5) do
    Process.send_after(self(), :alarm_off, 1000)
    {:noreply, state |> Map.put(:counter, counter + 1) |> Map.put(:st, IndigloOff) }
  else
    {:noreply, state |> Map.put(:counter, 0) |> Map.put(:st, IndigloOff) }
  end
 end

#  def handle_info(_msg, state) do
#   {:noreply, state}
#  end
def handle_info(:working, state), do: {:noreply, state}
def handle_info(:"bottom-left", state), do: {:noreply, state}
def handle_info(:"top-right", state), do: {:noreply, state}
def handle_info(:"top-left", state), do: {:noreply, state}
def handle_info(:start_alarm, state), do: {:noreply, state}
def handle_info(:alarm_off, state), do: {:noreply, state}
def handle_info(:stop_clock, state), do: {:noreply, state}
def handle_info(:"bottom-right", state), do: {:noreply, state}
def handle_info(:resume_clock, state), do: {:noreply, state}

end
